package com.zuitt.example;

//A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
//Packages are divided into two categories:
//1. Built - in Packages (Packages from the Java API)
//2. User-defined Packages (create your own packages)

//Package creation in Java follows the "reverse domain name notation" for the naming convention.
public class Variables {
    public static void main (String[] args){
        //Naming Convention
            // The terminology used for variable names is identifier.
            // All identifiers should begin with a letter (A - Z, a - z), currency ($) or an underscore (_).
            // After the first character, identifiers can have any combination of characters.
            // Most importantly Identifiers are case-sensitive

        //Variable Declaration:
        int age;
        char middleName;

        //Variable Declaration together with Initialization:
        int x;
        int y = 0;

        //Initialization after Declaration:
        x = 1; // x is already declared as int

        //Output to the System: (same as console)
        System.out.println("The value of y is " + y + " and the value of x " + x);

        //Primitive Data Types:
            // predefined within the Java Programming Language which is used for single-valued variables with limited capabilities

            // int - whole number values.
            int wholeNumber = 100;
            System.out.println(wholeNumber);

            //long - L is added to end of the long number to be recognized.
            long worldPopulation = 47821468712476L;
            System.out.println(worldPopulation);

            //Floating Value:
            //float - f is added at the end of the value. upto 7 decimal places
            float piFloat = 3.14159265359f;
            System.out.println("The value of piFloat is " + piFloat);
            //double
            double piDouble =  3.14159265359;
            System.out.println("The value of piDouble is " + piDouble);

            //char - single character
            //uses single quote
            char letter = 'a';
            System.out.println(letter);

            //boolean - true or false value
            boolean isLove = true;
            boolean isTaken = false;
            System.out.println(isLove);
            System.out.println(isTaken);

            //contants
            //Java uses the "final" keyword so that variable's value cannot be changed/reassigned.
            final int PRINCIPAL = 3000;
            //PRINCIPAL = 2999; //results to error
            System.out.println(PRINCIPAL);

        //Non-Primitive Data
          // also known as reference data types refers to instances or objects
          // does not directly store the value of a variable, but rather the reference to that variable
            //String
            //stores a sequence or array of characters
            //strings are actually object that can use methods
            String username = "JSmith";
            System.out.println(username);

            //sample string method ( .length() )
            int stringLength = username.length();
            System.out.println(stringLength);
    }
}
